const mongoose = require('mongoose');
const db = 'mongodb://localhost/douban-test';
mongoose.Promise = global.Promise;

exports.connect = () => {
	let MAX_CONNECT_TIMES = 0;
	return new Promise(((resolve, reject) => {
		if (process.env.NODE_ENV !== 'production') {
			mongoose.set('debug', true)
		}
		mongoose.connect(db);

		mongoose.connection.on('disconnected', () => {
			MAX_CONNECT_TIMES++;
			if (MAX_CONNECT_TIMES >3 ) {
				throw new Error('unknown mistake! Unable to connect to database');
			} else {
				mongoose.connect(db)
			}
		});
		mongoose.connection.on('error', err => {
			reject(err);
			console.log(err)
		});
		mongoose.connection.once('open',() => {
			resolve();
			console.log('MongoDB Connected successfully!')
		})
	}));

};