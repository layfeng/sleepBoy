const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Mixed = Schema.Types.Mixed; // mixed中可以存储各种类型的数据

const movieSchema = new Schema({
	doubanId: {
		type: String,
		unique: true
	},
	rate: Number,
	summary: String,
	title: String,
	video: String,
	poster: String,
	cover: String,

	rawTitle: String,
	movieTypes: [String],
	pubdate: Mixed,
	year: Number,

	tags: [String],

	meta: {
		cteatedAt: {
			type: Date,
			default: Date.now()
		},
		updatedAt: {
			type: Date,
			default: Date.now()
		}
	}
});
movieSchema.pre('save',next => {
	if (this.isNew) {
		this.meta.createdAt = this.meta.updatedAt = Date.now();
	}else {
		this.meta.updatedAt = Date.now();
	}
	next()
});
mongoose.model('Movie',movieSchema);
