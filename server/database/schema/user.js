const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Mixed = Schema.Types.Mixed; // mixed中可以存储各种类型的数据
const SALT_WORK_FACTOR = 10;
const MAX_LOGIN_ATTEMPTS = 5;
const LOCK_TIME = 2 * 60 * 60 * 1000;
const bcrypt = require('bcrypt');

const userSchema = new Schema({
	userName: {
		unique: true,
		type: String
	},
	email: {
		unique: true,
		tyep: String
	},
	password: {
		unique: true,
		type: String
	},
	lockUntil: { // 锁定到什么时候
		type: Number
	},
	loginAttempts: {
		type: Number,
		required: true,
		default: 0
	},
	meta: {
		cteatedAt: {
			type: Date,
			default: Date.now()
		},
		updatedAt: {
			type: Date,
			default: Date.now()
		}
	}
});
userSchema.virtual('isLocked').get(() => {
	return !!(this.lockUntil && this.lockUntil > Date.now());
});
// 加密中间件
userSchema.pre('save', next => {
	if (!this.isModified('password')) return next(); // (isModified)justify the password
	bcrypt.genSalt(SALT_WORK_FACTOR, (err, salt) => {
		if (err) return next(err);
		bcrypt.hash(this.password, salt, (error, hash) => {
			if (error) return next(error);
			this.password = hash;
			next() //将流程的控制权流出
		});
	});
	next()
});

userSchema.methods = {
	isLoginAttepts: (user) => {
		return new Promise(((resolve, reject) => {
			if (this.lockUntil && this.lockUntil < Date.now()) {
				this.update({
					$set: {
						loginAttempts: 1
					},
					$unset: {
						lockUntil: 1
					}
				}, (err) => {
					if (!err) resolve(true);
					else reject(err)
				})
			} else {
				let updates = {
					$inc:{
						loginAttempts: 1
					}
				};
				if (this.loginAttempts+1 >= MAX_LOGIN_ATTEMPTS && !this.isLocked) {
					updates.$set = {
						lockUntil: Date.now() + LOCK_TIME
					}
				}
				this.update(updates, err => {
					if (!err) resolve(true);
					else reject(err)
				})
			}
		}))
	},
	comparePassword: (_password, password) => {
		return new Promise(((resolve, reject) => {
			bcrypt.compare(_password, password, (err, isMatch) => {
				if (!err) resolve(isMatch);
				else reject(err)
			})
		}))
	}
};

mongoose.model('User', userSchema);
