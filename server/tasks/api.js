const rp = require('request-promise-native');

async function fetchMovie(item) {
	const url = `http://api.douban.com/v2/movie/subject/${ item.doubanId }`;

	const res = await rp(url);

	return res
}

!(async () => {
	let movies = [
		{
			doubanId: 1417598,
			title: '电锯惊魂',
			rate: 8.7,
			poster:
					'https://img3.doubanio.com/view/photo/l_ratio_poster/public/p2163771304.jpg'
		}, {
			doubanId: 1820156,
			title: '我是传奇',
			rate: 8,
			poster:
					'https://img3.doubanio.com/view/photo/l_ratio_poster/public/p574158935.jpg'
		}, {
			doubanId: 30424374,
			title: '爱，死亡和机器人 第一季',
			rate: 9.1,
			poster:
					'https://img3.doubanio.com/view/photo/l_ratio_poster/public/p2548248276.jpg'
		}, {
			doubanId: 1306809,
			title: '生化危机',
			rate: 8,
			poster:
					'https://img3.doubanio.com/view/photo/l_ratio_poster/public/p2174120073.jpg'
		}
	];
	movies.map(async movie => {
		let movieData = await fetchMovie(movie);
		try {
			movieData = JSON.parse(movieData);
			console.log(movieData.tags);
			console.log(movieData.summary)
		} catch (e) {
			console.log(e);
		}
	})
})();
