const Koa = require('koa');
const app = new Koa;
const { connect } = require('./database/data');

!(async () => {
    await connect();
})();

app.use(async (ctx,next) => {
    ctx.type = "text/html;charset=utf-8";
    ctx.body = 'Hi Luke'
});

app.listen(6626,() => {
    console.log('Server success...')
});
